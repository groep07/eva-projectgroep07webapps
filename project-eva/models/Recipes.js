var mongoose 	= require('mongoose');
var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
var fs          = require('fs');

var RecipeSchema = new mongoose.Schema({
    _id: ObjectId,
    title: String,
    difficultyRate: Number,
    ingredients: [
    {
        name: String,
        qty: String
    }
    ],
    description: [String],
    extraInfo: {
        preparationTime: Number,
        cookDifficulty: Number,
        recipeType: Number,
        dietTypes: [Number],
        isFastFood: Boolean
    },
    image: {
        data: Buffer, contentType: String
    }

});

mongoose.model('Recipe',RecipeSchema);