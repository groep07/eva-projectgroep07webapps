var mongoose 	= require('mongoose');
var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var UserSchema = new Schema({
	_id: ObjectId,
	emailaddress: String,
	name: String,
	lastName: String,
	password: String,
	vegetarianExperience: Number,
	familySituation: Number,
	newsletter: Boolean,
	score: Number,
	challenges: [
		{
			type: Schema.ObjectId, ref: 'Challenge'
		}
	],
	favoriteRecipes: [
		{
			type: Schema.ObjectId, ref: 'Recipe'
		}
	]
});

module.exports = mongoose.model('User', UserSchema, 'users');