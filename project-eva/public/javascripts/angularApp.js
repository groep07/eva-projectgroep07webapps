'use strict';

var app = angular
    .module('testApp', [
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch', 
        'ui.router',     
    ]);

app.config(function($routeProvider) {
    $routeProvider
        .when('/', {
                resolve: {
                    "check": function($cookies) {
                        if ($cookies.getObject('user')) {
                            window.location = "http://localhost:3000/challengeboard";
                        }
                    }
                },
                templateUrl: 'views/login.ejs',
                controller: 'InloginCtrl',
                controllerAs: 'inlog'
            })
        .when('profile', {
            templateUrl: 'views/profile.html',
                resolve: {
                    "check": function($cookies) {
                        if (!$cookies.getObject('user')) {
                            window.location = "http://localhost:3000/";
                        }
                        else {
                            alert("Geen toegang");
                        }
                    }
                },
                
                controller: 'ProfileCtrl',
                controllerAs: 'profile'
            })

        .when('/receptenboek',{
            resolve: {
                    "check": function($cookies) {
                        if (!$cookies.getObject('user')) {
                            window.location = "http://localhost:3000/";
                        }
                    }
                },
            templateUrl: 'views/receptenboek.ejs',

        }).when('/challengeboard',{
            templateUrl: 'views/challengeboard.ejs',
            controller: 'ChallengeboardCtrl',
            controllerAs: 'challengeboard'
        })

        .when('/challenge',{
            resolve: {
                    "check": function($cookies) {
                        if (!$cookies.getObject('user')) {
                            window.location = "http://localhost:3000/";
                        }
                    }
                },
            templateUrl: 'views/challenge.ejs',
            controller: 'ChallengeCtrl',
            controllerAs: 'challenge'
        })
        .when('/about',{
                        templateUrl: 'views/about.ejs',
            resolve: {
                    "check": function($cookies) {
                        if (!$cookies.getObject('user')) {
                            window.location = "http://localhost:3000/";
                        }
                        else {
                            alert("Geen toegang");
                        }
                    }
                }
        })
        .when('/recipe',{
            resolve: {
                    "check": function($cookies) {
                        if (!$cookies.getObject('user')) {
                            window.location = "http://localhost:3000/";
                        }
                    }
                },
            templateUrl: 'views/recipe.ejs',
            controller: 'RecipeCtrl',
            controllerAs: 'recipe'
        })
        .when('/register',{
            templateUrl: "views/register.ejs",
            controller: 'RegisterCtrl',
            controllerAs: 'register'
        })
    .otherwise({
        redirectTo: '/404.html'
    });
})

