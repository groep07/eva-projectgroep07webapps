var app = angular.module('testApp', ['ngCookies', 'ngDialog']);

app.directive('onLastRepeat', function() {
        return function(scope, element, attrs) {
            if (scope.$last) setTimeout(function(){
                scope.$emit('onRepeatLast', element, attrs);
            }, 1);
        };
    })

app.controller('ChallengeboardCtrl', 
    function($scope, $location, $rootScope, $http, $cookies, dataFactory, ngDialog) {

        $scope.recipe = "RecipeChallenge";
        $scope.resto = "RestaurantChallenge";
        $scope.fastfood = "FastfoodChallenge";
        $scope.social = "Sociaal";
        $scope.breakfast = "Breakfast";
        $scope.last = "Last";

        var user = $cookies.getObject('user'); 
        var challenges = [];

        for (challenge in user.data.challenges){
            dataFactory.getChallenges(user.data.challenges[challenge]).success(function(data) {
              challenges.push(data);

            }).error(function(error) {
              $scope.Status = 'Unable to load challenges data: ' + error.message;
            });
        }
        $scope.challenges = challenges;

        var favoriteRec = [];
        for (favorite in user.data.favoriteRecipes){
                dataFactory.getRecipesById(user.data.favoriteRecipes[favorite]).success(function(data) {
                  favoriteRec.push(data);
                }).error(function(error) {
                  $scope.Status = 'Unable to load challenges data: ' + error.message;
                });
        }
        $scope.favorites = favoriteRec;

        /* Load ng-repeat before calling isotope function */ 
        $scope.$on('onRepeatLast', function(scope, element, attrs){
          $(document).ready(function(){
            
            imagesLoaded( '.slider', function() {
                $('.slider').slick({
                  dots: true,
                  infinite: false,
                  centerMode: true,
                  centerPadding: '60px',
                  slidesToShow: 3,
                  slidesToScroll: 1,
                  responsive: [
                    {
                      breakpoint: 768,
                      settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 3
                      }
                    },
                    {
                      breakpoint: 480,
                      settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                      }
                    }
                  ]
                });
            });
          })
          });

//put id in localstorage to retrieve ID in challenge page
    $scope.showChallenge = function(challenge){
     localStorage.setItem('challenge',challenge);
    }

    $scope.showRecipe = function(recipeID) {
            localStorage.setItem('recipe', recipeID);
    }

    $scope.logout = function() {
        $http({
                    method: 'GET',
                    url: 'http://188.166.128.173:8080/api/logout',
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'Content-Type': 'application/json'
                    },
                    data: {
                        message: $scope.message
                    }
                }).then(function successCallback(response) {
                    console.log(response);

                    $cookies.remove('user');
                    alert("User logged out");
                    window.location = "http://localhost:3000/";
                }, function errorCallback(response) {
                    console.log("Failed to log out");
                });
    };

    $scope.showFavorites = function() {
            ngDialog.open({ 
                template: 'popupStart', 
                scope: $scope
                }); 
        };

});

app.factory('dataFactory', ['$http' ,'$cookies' ,function($http, $cookies){
    var dataFactory = {};

    var DbLink = 'http://188.166.128.173:8080/api/'; 
    dataFactory.getChallenges = function (id) {
        return $http.get(DbLink + 'challenges/' + id);
    };
    dataFactory.getRecipesById = function(id) {
        return $http.get(DbLink + 'recipes/' + id);
    };

    return dataFactory;
}]);