var app = angular.module('testApp', ['ngMessages']);

app.controller('RegisterCtrl', function($scope, $location, $rootScope, $http, dataFactory) {

        $scope.showSuccessAlert = false;

         $scope.vegExp = [
        { label: 'Omnivoor', value: 1},
        { label:'Pescotariër', value: 2},
        { label: 'Parttimevegetariër', value: 3},
        { label: 'Vegetariër', value: 4},
        { label: 'Veganist', value: 5}];

         $scope.famSit = [
        { label: 'Student', value: 1},
        { label:'Volwassene', value: 2},
        { label: 'Volwassene met kinderen', value: 3}];

        $scope.showDescription = function(){
          var index = $scope.user.vegetarianExperience;
             var descriptionVegExp = ['','Een omnivoor is geen vegetariër.',
         'Een pescotariër is een vegetariër met de uitzondering dat men wel vis en schaaldieren eet.',
         'Een parttime vegetariër is een vegetariër die 3 maal in de week vegetarisch eet.',
        'Een vegetariër eet vegetarisch d.w.z. geen vlees of vis.',
        'Een veganist eet geen producten van dierlijke oorsprong noch producten die hiermee zijn gemaak.'];
          $scope.desc =  descriptionVegExp[index];

        };

        $scope.submitForm = function() {

            var newUser = {
                name: $scope.user.name,
                lastName: $scope.user.lastName,
                emailaddress: $scope.user.emailaddress,
                password: $scope.user.password,
                vegetarianExperience: $scope.user.vegetarianExperience,
                familySituation: $scope.user.familySituation,
                newsletter: $scope.user.newsletter,
                score: "0",
                favoriteRecipes: [],
                challenges: []
            }
            dataFactory.register(newUser);
            $scope.showSuccessAlert = true;
        }
    }
);

app.factory('dataFactory', ['$http', function($http) {
    //link naar de database op de server
    var DbLink = 'http://188.166.128.173:8080/api/users';
    var dataFactory = {};

    dataFactory.register = function(newUser) {
        $http.post(DbLink, newUser);
    }

    return dataFactory;
}]);