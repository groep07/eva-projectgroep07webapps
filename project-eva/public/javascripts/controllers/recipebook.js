var app = angular.module('testApp', ['ngCookies', 'ngDialog']);

app.directive('onLastRepeat', function() {
    return function(scope, element, attrs) {
        if (scope.$last) setTimeout(function() {
            scope.$emit('onRepeatLast', element, attrs);
        }, 1);
    };
})

app.controller('RecipebookCtrl', function($scope, $location, $rootScope, $http,$cookies, dataFactory, ngDialog) {
        var aantal;
        var $container = $('.item-container');
        var iso;

        var user = $cookies.getObject('user');

        var favoriteRec = [];

        for (favorite in user.data.favoriteRecipes){
                dataFactory.getRecipesById(user.data.favoriteRecipes[favorite]).success(function(data) {
                  favoriteRec.push(data);
                }).error(function(error) {
                  $scope.Status = 'Unable to load challenges data: ' + error.message;
                });
            }
        $scope.favorites = favoriteRec;

        // alle recepten uit dat db halen met behulp van de factory
        dataFactory.getRecipes().success(function(data) {
            $scope.recipes = data;
        }).error(function(error) {
            $scope.status = 'Unable to load recipes data: ' + error.message;
        });

        /* Load ng-repeat before calling isotope function */
        $scope.$on('onRepeatLast', function(scope, element, attrs) {
            $(document).ready(function() {
                filters = {};
                imagesLoaded('.item-container', function() {
                    $container.isotope({
                        itemSelector: '.item',
                        layoutMode: 'fitRows'
                    }).isotopeSearchFilter();

                    iso = $container.data('isotope');
                    // count how many recipes
                    aantal = iso.filteredItems.length;
                    $scope.$apply(function() {
                        $scope.aantal = aantal;
                    });

                    // filter buttons
                    $('select').change(function() {
                        var $this = $(this);
                        console.log("dropdown werkt");

                        // store filter value in object
                        var group = $this.attr('data-filter-group');

                        filters[group] = $this.find(':selected').attr('data-filter-value');

                        // convert object into array
                        var isoFilters = [];
                        for (var prop in filters) {
                            isoFilters.push(filters[prop])
                        }

                        var selector = isoFilters.join('');
                        $container.isotope({
                            itemSelector: '.item',
                            layoutMode: 'fitRows',
                            filter: selector
                        }).isotopeSearchFilter();

                        iso = $container.data('isotope');
                        // count how many recipes
                        aantal = iso.filteredItems.length;
                        $scope.$apply(function() {
                            $scope.aantal = aantal;
                        });

                        return false;
                    });
                });
            })
        });

        //put id in localstorage to retrieve ID in recipe page
        $scope.showRecipe = function(recipeID) {
            localStorage.setItem('recipe', recipeID);
        }

        $scope.updateAmount = function() {
            setTimeout(function() {
                console.log("searchbar werkt");
                iso = $container.data('isotope');
                // count how many recipes
                aantal = iso.filteredItems.length;
                console.log("Aantal recepten: " + aantal);
                $scope.$apply(function() {
                    $scope.aantal = aantal;
                });
            }, 200);
        }

        $scope.showFavorites = function(challengeId) {
            ngDialog.open({ 
                template: 'popupStart', 
                scope: $scope
                }); 
        };

        $scope.logout = function() {
        $http({
                    method: 'GET',
                    url: 'http://188.166.128.173:8080/api/logout',
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'Content-Type': 'application/json'
                    },
                    data: {
                        message: $scope.message
                    }
                }).then(function successCallback(response) {
                    console.log(response);

                    $cookies.remove('user');
                    alert("User logged out");
                    window.location = "http://localhost:3000/";
                }, function errorCallback(response) {
                    console.log("Failed to log out");
                });
    };
    }
);








app.controller('RecipeCtrl', function($scope, $cookies, $location, $rootScope, $http, dataFactory, ngDialog) {
    var recipeID = localStorage.getItem('recipe');
    var user = $cookies.getObject('user');

    var favoriteRec = [];

    for (favorite in user.data.favoriteRecipes){
                dataFactory.getRecipesById(user.data.favoriteRecipes[favorite]).success(function(data) {
                  favoriteRec.push(data);
                }).error(function(error) {
                  $scope.Status = 'Unable to load challenges data: ' + error.message;
                });
            }
        $scope.favorites = favoriteRec;
    $scope.favorites = favoriteRec;

    dataFactory.getRecipesById(recipeID).success(function(data) {
        $scope.recipe = data;
        $scope.preparationTime = changePreparationTimeToString(data.extraInfo.preparationTime);
        $scope.cookDifficulty = changeCookDifficultyToString(data.extraInfo.cookDifficulty);
        $scope.recipeType = changeRecipeTypeToString(data.extraInfo.recipeType);
        $scope.recipeInfoDietType = changeRecipeInfoDietType(data.extraInfo.dietTypes);
        $scope.checkFavorite();
    }).error(function(error) {
        $scope.status = 'Unable to load recipes data: ' + error.message;
    });
    $scope.checkFavorite = function() {
        if (user.data.favoriteRecipes.indexOf(recipeID) === -1) {
            $scope.favoriteImgUrl = "/images/heart_black.png";
        } else {
            $scope.favoriteImgUrl = "/images/heart_red.png";
        }
    }

    $scope.favorite = function(recipeID) {
        dataFactory.getUserById(user.data._id).success(function(data) {
            if (data.favoriteRecipes.indexOf(recipeID) === -1) {
                var favoriteRec = data.favoriteRecipes.concat(recipeID);
                $scope.favoriteImgUrl = "/images/heart_red.png";
            } else {
                data.favoriteRecipes.splice(data.favoriteRecipes.indexOf(recipeID), 1);
                var favoriteRec = data.favoriteRecipes;
                console.log(favoriteRec);
                $scope.favoriteImgUrl = "/images/heart_black.png";
            }

            var newUser = {
                name: user.data.name,
                lastName: user.data.lastName,
                emailaddress: user.data.emailaddress,
                password: user.data.password,
                vegetarianExperience: user.data.vegetarianExperience,
                familySituation: user.data.familySituation,
                newsletter: user.data.newsletter,
                score: user.data.score,
                favoriteRecipes: favoriteRec,
                challenges: user.data.challenges
            }

            dataFactory.addFavorite(newUser, user.data._id);
        });
        $scope.checkFavorite();
    };
    $scope.logout = function() {
        $http({
                    method: 'GET',
                    url: 'http://188.166.128.173:8080/api/logout',
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'Content-Type': 'application/json'
                    },
                    data: {
                        message: $scope.message
                    }
                }).then(function successCallback(response) {
                    console.log(response);

                    $cookies.remove('user');
                    alert("User logged out");
                    window.location = "http://localhost:3000/";
                }, function errorCallback(response) {
                    console.log("Failed to log out");
                });
    };
      $scope.showRecipe = function(recipeID) {
        localStorage.setItem('recipe', recipeID);
        window.location = "http://localhost:3000/recipe";
        };

        $scope.showFavorites = function(challengeId) {
            ngDialog.open({ 
                template: 'popupStart', 
                scope: $scope
                }); 
        };

});


// de preparatietijd die in de db als een getal staat omzetten naar tekst
function changePreparationTimeToString(prepTime) {
    var preparationTime = ['Lang', 'Medium', 'Snel'];
    return preparationTime[prepTime - 1];
};

// de moeilijkheidsgraad van het recept die in de db als een getal staat omzetten naar tekst
function changeCookDifficultyToString(cookDif) {
    var cookDifficulty = ['Voor starters', 'Voor enthousiasten', 'Voor koks'];
    return cookDifficulty[cookDif - 1];
};

// het type van het recept die in de db als een getal staat omzetten naar tekst
function changeRecipeTypeToString(recipeTyp) {
    var recipeType = ['Broodbeleg', 'Drank', 'Hapje', 'Soep', 'Voorgerecht', 'Hoofdgerecht',
        'Saus', 'Bijgerecht', 'Nagerecht'
    ];
    return recipeType[recipeTyp - 1];
};

// de alergie die in de db als een getal staat omzetten naar tekst
function changeRecipeInfoDietType(recipeInfoDietTyp) {
    var recipeInfoDietType = ['Vegetarisch', 'Glutenvrij', 'Suikervrij', 'Kindvriendelijk'];
    var recipeInfoDietTypeString = '';
    for (type in recipeInfoDietTyp) {
        recipeInfoDietTypeString += recipeInfoDietType[type] + ' ';
    }
    return recipeInfoDietTypeString;
};

app.factory('dataFactory', ['$http', function($http) {
    //link naar de database op de server
    var DbLink = 'http://188.166.128.173:8080/api';
    var dataFactory = {};

    dataFactory.getRecipes = function() {
        return $http.get(DbLink + '/recipes');
    };

    dataFactory.getRecipesById = function(id) {
        return $http.get(DbLink + '/recipes/' + id);
    };

    dataFactory.addFavorite = function(user, userID) {
        $http.put(DbLink + "/users/" + userID, user);
    };

    dataFactory.getUserById = function(id) {
        return $http.get(DbLink + "/users/" + id);
    };
    return dataFactory;
}]);