var app = angular.module('testApp', ['ngCookies','ngDialog']);
var challengeID = localStorage.getItem('challenge');
var challengeType = localStorage.getItem('challengeType');
var dietTypeID = localStorage.getItem('dietType');
var challenge = localStorage.getItem('challengeID');


app.controller('ChallengeCtrl', function($scope, $location, $rootScope, $http, $cookies, dataFactory, ngDialog) {

    //Set recipe divs hidden on default
    $scope.VeggieHidden = true;
    $scope.GlutenHidden = true;
    $scope.SugarHidden = true;
    $scope.ChildHidden = true;

   

    $scope.ShowVegetarian = function() {
        //If DIV is hidden it will be visible and vice versa.
        $scope.VeggieHidden = $scope.VeggieHidden ? false : true;
    }
    $scope.ShowGlutenfree = function() {
        //If DIV is hidden it will be visible and vice versa.
        $scope.GlutenHidden = $scope.GlutenHidden ? false : true;
    }
    $scope.ShowSugarfree = function() {
        //If DIV is hidden it will be visible and vice versa.
        $scope.SugarHidden = $scope.SugarHidden ? false : true;
    }
    $scope.ShowChildFriendly = function() {
        //If DIV is hidden it will be visible and vice versa.
        $scope.ChildHidden = $scope.ChildHidden ? false : true;
    }

    // challenge object uit localStorage halen
    dataFactory.getChallenge(challengeID).success(function(data) {
        $scope.challenge = data;
        $scope.state = data.state;
        console.log($scope.state);
        dataFactory.getChallengeType(data.challengeType).success(function(data) {
            $scope.challengeType = data;
            $scope.type = $scope.challengeType._type;
            $scope.recipe = "RecipeChallenge";
            $scope.resto = "RestaurantChallenge";
            $scope.fastfood = "FastfoodChallenge";
            dataFactory.getDietTypeMaker(data.dietTypeMaker).success(function(data) {
                $scope.dietTypeMaker = data;
                dataFactory.getRecipes(data.childFriendly.recipe).success(function(data) {
                    $scope.childFriendly = data;
                })
                dataFactory.getRecipes(data.sugarFree.recipe).success(function(data) {
                    $scope.sugarFree = data;
                })
                dataFactory.getRecipes(data.glutenFree.recipe).success(function(data) {
                    $scope.glutenFree = data;
                })
                dataFactory.getRecipes(data.vegetarian.recipe).success(function(data) {
                    $scope.vegetarian = data;
                })
            })
        })
    }).error(function(error) {
        $scope.status = 'Unable to load challenge data: ' + error.message;
    });

    // only get te restaurants and cities if it is a restaurantChallenge
    if ($scope.type = "RestaurantChallenge") {

        dataFactory.getCities().success(function(data) {
            $scope.cities = data;
        });
        dataFactory.getRestaurants().success(function(data) {
            $scope.restaurants = data;
        });
    };

    var user = $cookies.getObject('user');
    var favoriteRec = [];

    for (favorite in user.data.favoriteRecipes){
                dataFactory.getRecipes(user.data.favoriteRecipes[favorite]).success(function(data) {
                  favoriteRec.push(data);
                }).error(function(error) {
                  $scope.Status = 'Unable to load challenges data: ' + error.message;
                });
    }
    $scope.favorites = favoriteRec;

    $scope.StartChallenge = function(challengeId) {
        changeStateOfChallenge(challengeId, 4);
        ngDialog.open({ 
            template: 'popupStart', 
            closeByEscape: false,
            showClose: false,
            closeByDocument: false
        }); 
    };

    $scope.showFavorites = function() {
            ngDialog.open({ 
                template: 'popupFavorites', 
                scope: $scope
                }); 
    };

    $scope.showRecipe = function(recipeID) {
            localStorage.setItem('recipe', recipeID);
    };

    $scope.StopChallenge = function(challengeId) {
        //challenge completed
        changeStateOfChallenge(challengeId, 6);

        var huidigeChallengeIndex = user.data.challenges.indexOf($scope.challenge._id);
        console.log("Huidige ID: " + $scope.challenge._id);

        var availableChallenge = user.data.challenges[huidigeChallengeIndex + 1];
        console.log("To available: " + availableChallenge);

        var visibleChallenge = user.data.challenges[huidigeChallengeIndex + 2];
        console.log("To visible: " + visibleChallenge);

        if(user.data.challenges[20] = huidigeChallengeIndex){
            $scope.finalMessage = "Proficiat je hebt alle challenges voltooid";
        };

        //change state of the next challenge to available
        changeStateOfChallenge(availableChallenge, 3);
        // change state of challenge after the next to visible
        changeStateOfChallenge(visibleChallenge, 2);
        ngDialog.open({ 
            template: 'popupCompleted',
            closeByEscape: false,
            showClose: false,
            closeByDocument: false
         });
    };

    changeStateOfChallenge = function(id, state) {
        dataFactory.getChallenge(id).success(function(data) {
            $scope.chall = data;

            var changeChallenge = {
                _id: id,
                name: $scope.chall.name,
                description: $scope.chall.description,
                challengeType: $scope.chall.challengeType,
                typeOfChallenge: $scope.chall.typeOfChallenge,
                state: state,
                difficulty: $scope.chall.difficulty
            }
            dataFactory.changeChallengeState(id, changeChallenge);
        });
        
    };

    $scope.showRecipe = function(recipeID) {
        localStorage.setItem('recipe', recipeID);
    }
     $scope.logout = function() {
        $http({
                    method: 'GET',
                    url: 'http://188.166.128.173:8080/api/logout',
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'Content-Type': 'application/json'
                    },
                    data: {
                        message: $scope.message
                    }
                }).then(function successCallback(response) {
                    console.log(response);

                    $cookies.remove('user');
                    alert("User logged out");
                    window.location = "http://localhost:3000/";
                }, function errorCallback(response) {
                    console.log("Failed to log out");
                });
    };
});

app.factory('dataFactory', ['$http', function($http) {
    //link naar de database op de server
    var DbLink = 'http://188.166.128.173:8080/api/';
    var dataFactory = {};

    dataFactory.getChallenge = function(id) {
        return $http.get(DbLink + 'challenges/' + id);
    };

    dataFactory.getChallengeType = function(id) {
        return $http.get(DbLink + 'challengeTypes/' + id);
    };

    dataFactory.getDietTypeMaker = function(id) {
        return $http.get(DbLink + 'dietTypeMakers/' + id);
    };

    dataFactory.getRecipes = function(id) {
        return $http.get(DbLink + 'recipes/' + id);
    };

    dataFactory.getCities = function() {
        return $http.get(DbLink + "cities");
    };

    dataFactory.getRestaurants = function() {
        return $http.get(DbLink + "restaurants");
    };


    dataFactory.changeChallengeState = function(id, challenge) {
        $http.put(DbLink + "challenges/" + id, challenge);
    };

    return dataFactory;
}]);