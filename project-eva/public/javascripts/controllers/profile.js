var app = angular.module('testApp', ['ngMessages', 'ngCookies']);

app.controller('ProfileCtrl', 
    function($scope, $location, $rootScope, $http, $cookies, dataFactory) {

        var user = $cookies.getObject('user');
        $scope.showSuccessAlert = false;
        $scope.user = user.data;

         $scope.vegExp = [
        { label: 'Omnivoor', value: 1},
        { label:'Pescotariër', value: 2},
        { label: 'Parttimevegetariër', value: 3},
        { label: 'Vegetariër', value: 4},
        { label: 'Veganist', value: 5}];

         $scope.famSit = [
        { label: 'Student', value: 1},
        { label:'Volwassene', value: 2},
        { label: 'Volwassene met kinderen', value: 3}];

        $scope.fullName = $scope.user.name + " " + $scope.user.lastName;

        $scope.splitUserName = function() {
            var fullNameString = $scope.fullName;
            var splittedName = fullNameString.split(/ (.+)?/);
            var firstname = splittedName[0];
            var lastname = splittedName[1];

            $scope.user.name = firstname;
            $scope.user.lastName = lastname;
        };
          
        $scope.changeForm = function(){
        dataFactory.change($scope.user._id,$scope.user);
        $scope.showSuccessAlert = true;
        }

        $scope.logout = function() {
        $http({
                    method: 'GET',
                    url: 'http://188.166.128.173:8080/api/logout',
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'Content-Type': 'application/json'
                    },
                    data: {
                        message: $scope.message
                    }
                }).then(function successCallback(response) {
                    console.log(response);

                    $cookies.remove('user');
                    alert("User logged out");
                    window.location = "http://localhost:3000/";
                }, function errorCallback(response) {
                    console.log("Failed to log out");
                });
    };
    }
);

app.factory('dataFactory', ['$http', function($http) {
    //link naar de database op de server
    var DbLink = 'http://188.166.128.173:8080/api/users';
    var dataFactory = {};

    dataFactory.register = function(newUser) {
        $http.post(DbLink, newUser);
    }

    dataFactory.change = function(userId, user){
        $http.put(DbLink + "/" + userId, user);
    };

    return dataFactory;
}]);