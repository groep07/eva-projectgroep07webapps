var app = angular.module('testApp', ['ngCookies']);

app.controller('InlogCtrl',
    function($cookies, $scope, $rootScope, $http) {
    "use strict";

    $scope.login = function() {
        if ($scope.username === 'test' && $scope.password === 'test') {
            $rootScope.loggedIn = true;
            $rootScope.username = $scope.username;

            window.location = "http://localhost:3000/challengeboard";
        } else {
            if ($scope.username !== null && $scope.password !== null) {

                $http({
                    method: 'POST',
                    url: 'http://188.166.128.173:8080/api/login',
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'Content-Type': 'application/json'
                    },
                    data: {
                        username: $scope.username,
                        password: $scope.password
                    }
                }).then(function successCallback(response) {
                    $cookies.putObject('user', response);

                    window.location = "http://localhost:3000/challengeboard";

                    /* You can set the expired time with the third params
                    var today = new Date();
                    var expired = new Date(today);
                    expired.setDate(today.getDate() + 1); //Set expired date to tomorrow
                    $cookies.put('user', user.username, {expires : expired });
                    */
                   
                }, function errorCallback(response) {
                    // console.log(response);
                    // show error message
                    if (response.data !== null) {
                        $scope.error = response.data.message;
                        $scope.foutmelding = "Het ingegeven wachtwoord is foutief";
                    } else {
                        // $scope.error = response.data.message;
                        window.location = "http://localhost:3000/";
                    }
                });
            } else {
                $scope.error = "Gelieve alle gegevens in te vullen.";
            }
        }
    };
});