var app = angular.module('testApp', ['ngCookies']);

app.controller('AboutCtrl',
    function($cookies, $scope, $rootScope, $http) {
    "use strict";

    $scope.logout = function() {
        $http({
                    method: 'GET',
                    url: 'http://188.166.128.173:8080/api/logout',
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'Content-Type': 'application/json'
                    },
                    data: {
                        message: $scope.message
                    }
                }).then(function successCallback(response) {
                    console.log(response);

                    $cookies.remove('user');
                    alert("User logged out");
                    window.location = "http://localhost:3000/";
                }, function errorCallback(response) {
                    console.log("Failed to log out");
                });
    };

});