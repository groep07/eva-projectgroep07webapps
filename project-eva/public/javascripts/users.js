var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
    email: { type: String, required: true, unique: true },
    hash: String,
    salt: String,
    name: String,
    lastName: String,
    vegetarianExperience: String,
    familySituation: String,
    newsletter: Boolean,
    score: {type:Number,Default:0},
    currentChallenge: Number,
    dateOfBirth: {type:Date,Default:Date.now}
});
