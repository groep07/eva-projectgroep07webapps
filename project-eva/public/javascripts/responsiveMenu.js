          $(function() {
          var pull        = $('#pull');
              menu        = $('#menuRes');
              menuHeight  = menu.height();

          $(pull).on('click', function(e) {
            e.preventDefault();
            menu.slideToggle();
          });

          $(window).resize(function(){
                var w = $(window).width();
                if(w > 835) {
                  menu.removeAttr('style');
                }
            });
        });