var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Recipe = mongoose.model('Recipe');
var User = mongoose.model('User');
var jwt = require('express-jwt');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('inlog', { title: 'Express' });
});

router.post('/register' ,function(req,res,next){
	var user = new User();
	user.emailaddress = req.body.emailaddress;
	user.name = req.body.name;
	user.lastName = req.body.lastName;
	user.password = req.body.password;
	user.vegetarianExperience = req.body.vegetarianExperience;
	user.familySituation = req.body.familySituation;
	user.newsletter = req.body.newsletter;
	user.score = 0;

	user.save(function(err){
		if(err){return next (err);}
		return res.json({token: user.generateJWT()})
	});
});

/* GET profile page */
router.get('/profile',function(req, res, next){
	res.render('profile',{ title: 'Profile'});
});

/* GET challengeboard page */
router.get('/challengeboard',function(req, res, next){
	res.render('challengeboard',{title: 'Challengeboard'});
});

router.get('/receptenboek',function(req, res, next){
	res.render('receptenboek',{title: 'Receptenboek'});
});

/* GET challenge page */
router.get('/challenge', function(req, res, next){
	res.render('challenge',{title: 'Challenge'});
});

router.get('/about', function(req, res, next){
  res.render('about',{title: 'About'});
});

router.get('/recipe', function(req, res, next){
  res.render('recipe',{title: 'Recipe'});
});

router.get('/register', function(req, res, next){
  res.render('register',{title: 'Register'});
});


module.exports = router;
